import { types } from "../types/types";
import {Task} from "../entity/Response";

const initialState : {
    tasks: Task[],
    tasksLoaded: boolean,
} = {
    tasks: [],
    tasksLoaded: false,
}

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.taskCreated:
            return {
                ...state,
                tasks: [
                    ...state.tasks,
                    {...action.payload}
                ],
            };
        case types.taskListLoaded:
            return {
                ...state,
                tasksLoaded: true,
                tasks: [
                    ...action.payload
                ],
            };
        case types.taskUpdated:
            return {
                ...state,
                tasksLoaded: true,
                tasks: state.tasks.map((task) => (task.id === action.payload.id ? {...action.payload} : {...task}))
            };
    default:
        return state;
    }
}

export default taskReducer;