export class Response {
    data: any;
    success:boolean;
}

export class Task {
    id: number;
    name: string;
    author: string;
    createdAt: string;
    description: string;
    isComplete: boolean;
    updatedAt: string
}

export class ResponseTask extends Response {
    data: Task;
}

export class ResponseTaskList extends Response {
    data: Task[];
}