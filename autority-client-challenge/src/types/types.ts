export const types = {
    taskCreated: '[task] Created Task',
    taskUpdated: '[task] Updated Task',
    taskListLoaded: '[task] Task List Loaded',
    taskSelected: '[task] Task Selected',
}