import TaskService from "../service/task.service";
import {types} from "../types/types";
import {ResponseTask, ResponseTaskList, Task} from "../entity/Response";

const service = new TaskService();


export const createTask = (task, onSuccess) => {
    return async (dispatch) => {
        try {

            const response: ResponseTask= await service.create(task);
            dispatch({
                type: types.taskCreated,
                payload: response.data
            });

            onSuccess(response.data);

        } catch (error) {
            alert(error.message ?? 'Something went wrong');
        }
    }
}


export const updateTask = (task, onSuccess=undefined) => {
    return async (dispatch) => {
        try {

            const response: ResponseTask= await service.update(task.id, task);
            dispatch({
                type: types.taskUpdated,
                payload: response.data
            });

            if(onSuccess){
                onSuccess(response.data);
            }

        } catch (error) {
            alert(error.message ?? 'Something went wrong');
        }
    }
}



export const loadAllTasks = () => {
    return async (dispatch) => {
        try {

            const response: ResponseTaskList = await service.getAll();

            dispatch({
                type: types.taskListLoaded,
                payload: response.data
            });

        } catch (error) {
            alert(error.message ?? 'Something went wrong');
        }
    }
}

export const loadOneTask = async (id: number) : Promise<Task> => {
    try {

        const response: ResponseTask = await service.getOne(id);

        return response.data;

    } catch (error) {
        alert(error.message ?? 'Something went wrong');
    }
    return undefined;

}

export const selectTask = (task: Task) => ({
        type: types.taskSelected,
        payload: task
    })