import type { NextPage } from 'next'
import Head from 'next/head'

import styles from '../styles/Home.module.css'
import {useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../app/hooks";
import {loadAllTasks, selectTask, updateTask} from "../actions/taskAction";
import {useSelector} from "react-redux";
import {Button, Col, Container, Row, Table} from "react-bootstrap";
import {useRouter} from "next/router";
import {Task} from "../entity/Response";
import Link from "next/link";

const IndexPage: NextPage = () => {
    const dispatch = useAppDispatch();
    const router = useRouter();

    useEffect(() => {
        dispatch(loadAllTasks());
    }, []);

    const { tasks } = useAppSelector(state => state.task)

    const handleTaskSelected = (task) => {
        router.push(`/task/${task.id}`);
    }

    const markAsCompleted = (task: Task) => {
        const completedTask = {...task, isComplete: true};

        dispatch(updateTask(completedTask));
    }

  return (
    <div className={styles.container}>
      <Head>
        <title>Autority Challenge</title>
      </Head>
        <Container fluid>

            <Row >
                <Col xs={{span: 8, offset: 2}} offset={2} className="text-center">
                    <h1>Task list</h1>
                </Col>
                <Col xs={{span: 2}} offset={2} className="text-center">
                    <Link className="btn btn-success" href="/task">New</Link>
                </Col>
            </Row>
            <Row >
                <Col>
                    <Table  striped bordered>
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Description
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>

                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        {tasks.map((task) => (
                            <tr key={task.id}>
                                <td>
                                    {task.id}
                                </td>
                                <td>
                                    {task.name}
                                </td>
                                <td>
                                    {task.description}
                                </td>
                                <td>
                                    {task.isComplete ? 'Done': (
                                        <Button onClick={() => markAsCompleted(task)} >Mark complete</Button>
                                    )}
                                </td>
                                <td>
                                    <Button onClick={() => handleTaskSelected(task)} >Edit</Button>
                                </td>
                            </tr>
                        ))}
                        {tasks.length === 0 && (
                            <tr>
                                <td colSpan={5}>
                                    You don't have any tasks created yet.<br />
                                    <Link className="btn btn-success" href="/task">Create a new one</Link>

                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
    </div>
  )
}

export default IndexPage
