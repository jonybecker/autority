import {NextPage} from "next";
import Head from "next/head";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import taskService from "../../service/task.service";
import {createTask, loadOneTask, updateTask} from "../../actions/taskAction";
import {Task} from "../../entity/Response";
import {Button, Col, Container, Form, Row} from "react-bootstrap";
import {useAppDispatch, useForm} from "../../app/hooks";
import Link from "next/link";

const TasksPage: NextPage = () => {
    const router = useRouter();
    const { id } = router.query;

    const [task, setTask] = useState<Task>();

    const dispatch = useAppDispatch();

    const onSuccess = () => {
        alert('Task successfully saved');
        router.push('/');
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        dispatch(updateTask(task, onSuccess));
    }

    useEffect(() => {
        if(!id){
            return;
        }
        const loadTask = async (id) => {
            const response = await loadOneTask(id as number);

            if(!response){
                router.push('/')
                return;
            }

            setTask(response);
        }

        loadTask(id);
    }, [id]);

    if(!task){
        return <>Loading</>
    }

    return (
        <div >
            <Container fluid>
                <Row  fluid className="mt-3">
                    <Col xs={{span: 2}}  className="text-center">
                        <Link className="btn btn-info" href="/">Back</Link>
                    </Col>
                    <Col xs={{span: 8}}  className="text-center">
                        <h1>Update task #{task.id}</h1>
                    </Col>
                </Row>
                <Row>
                    <Col xs={{span:6, offset: 3}}>
                        <Form onSubmit={handleSubmit} className="text-center">
                            <Form.Group>
                                <Form.Control type="text" name="name" placeholder="Task name"
                                              minLength={3}
                                              required={true}
                                              value={task.name}
                                              onChange={(e) => setTask((prevState) => ({
                                                    ...prevState,
                                                    name: e.target.value
                                                }))}
                                />
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="description" placeholder="Description"
                                              minLength={5}
                                              required={true}
                                              value={task.description}
                                              onChange={(e) => setTask((prevState) => ({
                                                  ...prevState,
                                                  description: e.target.value
                                              }))}
                                />
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="author" placeholder="Author"
                                              minLength={3}
                                              required={true}
                                              value={task.author}
                                              onChange={(e) => setTask((prevState) => ({
                                                  ...prevState,
                                                  author: e.target.value
                                              }))}
                                />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>
                                    <Form.Check name="isComplete"
                                                value={1}
                                                checked={task.isComplete}
                                                onChange={(e) => setTask((prevState) => ({
                                                        ...prevState,
                                                        isComplete: e.target.checked
                                                    }))}
                                    />
                                    Is Completed
                                </Form.Label>
                            </Form.Group>
                            <Button type="submit">Update</Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default TasksPage