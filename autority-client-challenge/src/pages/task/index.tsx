import {NextPage} from "next";
import Head from "next/head";
import {useAppDispatch, useForm} from "../../app/hooks";
import {Container, Row, Col, Form, Button} from "react-bootstrap";
import {createTask} from "../../actions/taskAction";
import {useRouter} from "next/router";
import Link from "next/link";

const TasksPage: NextPage = () => {
    const router = useRouter();
    const dispatch = useAppDispatch();

    const onSuccess = () => {
        router.push('/');
    }
    const handleSubmit = (formValues) => {
        dispatch(createTask(formValues, onSuccess));
    }

    const handler = useForm({
        name: '',
        description: '',
        author: '',
        isComplete: false
    });

    return (
        <>
            <Head>
                <title>Autority Challenge</title>
            </Head>

            <Container>
                <Row  fluid className="mt-3">
                    <Col xs={{span: 2}}  className="text-center">
                        <Link className="btn btn-info" href="/">Back</Link>
                    </Col>
                    <Col xs={{span: 8}}  className="text-center">
                        <h1>Create task</h1>
                    </Col>
                </Row>
                <Row >
                    <Col xs={{span: 6, offset: 3}}>
                        <Form onSubmit={handler(handleSubmit)} className="text-center">
                            <Form.Group>
                                <Form.Control type="text" name="name" placeholder="Task name" minLength={3} required={true}/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="description" placeholder="Description" minLength={5} required={true}/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="author" placeholder="Author" minLength={3} required={true}/>
                            </Form.Group>
                            <Button type="submit">Submit</Button>
                        </Form>
                    </Col>
                </Row>
            </Container>

        </>
    )
}

export default TasksPage