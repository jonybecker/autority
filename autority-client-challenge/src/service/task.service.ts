import ApiService from "./api.service";
import {ResponseTask, ResponseTaskList, Task} from "../entity/Response";

class TaskService {
    private apiService = new ApiService();

    async create(task: Task) : Promise<ResponseTask> {
        return await this.apiService.post(`task`, task) as unknown as ResponseTask;
    }

    async getAll(): Promise<ResponseTaskList> {
        return await this.apiService.get(`tasks`) as unknown as ResponseTaskList;
    }

    async getOne(id: number): Promise<ResponseTask> {
        return await this.apiService.get(`task/${id}`) as unknown as ResponseTask;
    }

    async update(id: number, task: Task) {
        return await this.apiService.put(`task/${id}`, task) as unknown as ResponseTask;
    }
}

export default TaskService;