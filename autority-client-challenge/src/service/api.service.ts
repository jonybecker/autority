const baseUrl = `http://localhost:4000`

class ApiService {

    async send (endpoint, data = {},method = 'GET', headers = {}): Promise<Response>
    {
        const url = `${baseUrl}/${endpoint}`;

        headers['Accept'] = 'application/json';
        headers['Content-Type'] = 'application/json';

        const options = {
            method,
            headers,
            body: undefined,
        }

        if(!['GET', 'HEAD'].includes(method)){
            options.body = JSON.stringify(data);
        }

        const response = await fetch(url, options);

        return <Response> await response.json();
    }

    async get (endpoint): Promise<Response> {
        return await this.send(endpoint);
    }

    async post (endpoint, data = {}): Promise<Response> {
        return await this.send(endpoint, data, 'POST');
    }

    async put (endpoint, data): Promise<Response> {
        return await this.send(endpoint, data, 'PUT');
    }

    async delete (endpoint, data): Promise<Response> {
        return await this.send(endpoint, data, 'DELETE');
    }

    async patch (endpoint, data): Promise<Response> {
        return await this.send(endpoint, data, 'PATCH');
    }
}

export default ApiService;