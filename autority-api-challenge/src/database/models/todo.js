const {
  DataTypes, Model,
} = require('sequelize');

module.exports = (sequelize) => {
  class Todo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }

  Todo.init({
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    author: DataTypes.STRING,
    isComplete: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'todo',
  });
  return Todo;
};
