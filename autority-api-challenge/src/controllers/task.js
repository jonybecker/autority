import db from '@/database';
import { matchedData } from 'express-validator';

/**
 * GET /tasks
 * Get all Task created
 */
export const getAllTasks = async (req, res) => {
  const tasks = await db.models.todo.findAll({
    order: [
      ['id', 'ASC'],
    ],
  });
  return res.json({ success: true, data: tasks });
};

/**
 * GET /task/{id}
 * Get one task by id
 */
export const getTask = async (req, res) => {
  const { taskId } = req.params;
  const task = await db.models.todo.findOne({ where: { id: taskId } });

  if (!task) {
    return res.status(404).json({
      success: false,
    });
  }

  return res.json({ success: true, data: task });
};

/**
 * POST /task
 * Create Task
 */
export const createTask = async (req, res) => {
  const {
    name,
    description,
    author,
    isComplete,
  } = matchedData(req);

  const task = await db.models.todo.create({
    name,
    description,
    author,
    isComplete,
  });

  return res.json({ success: true, data: task });
};
/**
 * PUT /tasks
 * Updates a task by id
 */
export const updateTask = async (req, res) => {
  const { taskId } = req.params;
  const task = await db.models.todo.findOne({ where: { id: taskId } });

  if (!task) {
    return res.status(404).json({
      success: false,
    });
  }
  const {
    name,
    description,
    author,
    isComplete,
  } = matchedData(req);

  await task.update({
    name,
    description,
    author,
    isComplete,
  });

  return res.json({ success: true, data: task });
};

/**
 * DELETE /tasks
 * Delete a task by id
 */
export const deleteTask = async (req, res) => {
  const { taskId } = req.params;
  const task = await db.models.todo.findOne({ where: { id: taskId } });

  if (!task) {
    return res.status(404).json({
      success: false,
    });
  }

  await task.destroy();

  return res.status(204).json({
    success: true,
  });
};
