import { Router } from 'express';

import * as homeController from '@/controllers/home';
import * as taskController from '@/controllers/task';

import * as validator from '@/validator';
import { validateRequest } from '@/helpers';

const router = Router();

router.get('/', homeController.index);

router.get('/health', homeController.healthCheck);

router.get('/tasks', taskController.getAllTasks);
router.get('/task/:taskId', taskController.getTask);
router.post('/task', validateRequest(validator.createTask), taskController.createTask);
router.delete('/task/:taskId', taskController.deleteTask);
router.put('/task/:taskId', validateRequest(validator.updateTask), taskController.updateTask);

export default router;
