import { validationResult } from 'express-validator';
// can be reused by many routes

// parallel processing
const validateRequest = (validations) => async (req, res, next) => {
  await Promise.all(validations.map((validation) => validation.run(req)));

  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }

  return res.status(400).json({
    success: false,
    errors: errors.array(),
  });
};

export default validateRequest;
