import { check } from 'express-validator';

export const createTask = [
  check('name').isLength({ min: 3 }),
  check('description').isLength({ min: 5 }),
  check('author').isLength({ min: 3 }),
  check('isComplete').isBoolean(),
];

export const updateTask = [
  check('name').isLength({ min: 3 }),
  check('description').isLength({ min: 5 }),
  check('author').isLength({ min: 3 }),
  check('isComplete').isBoolean(),
];
